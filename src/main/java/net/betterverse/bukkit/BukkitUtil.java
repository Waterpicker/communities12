/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.bukkit;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Admin
 */
public class BukkitUtil {

	public static Set<Player> getBukkitPlayersByName(Server server, Collection<String> names) {
		Set<Player> players = new HashSet<Player>();
		for (String name : names) {
			Player player = server.getPlayer(name);
			if (player != null) {
				players.add(player);
			}
		}
		return players;
	}

	public static void sendTo(CommandSender sender, String msg, Object... args) {
		if (sender == null) {
			return;
		}

		if (args.length == 0) {
			sender.sendMessage(msg);
		} else {
			sender.sendMessage(String.format(msg, args));
		}
	}
}
