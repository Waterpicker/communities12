/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.community;

import com.iciql.Db;
import net.betterverse.communities.DbAddress;

/**
 *
 * @author Admin
 */
public class DbManager {

	private CoreManager core;
	private Db db;
	private DbLogin dbLogin = new DbLogin(new DbAddress(DbAddress.DbBackend.H2, "mem:communities").toString());

	public DbManager(CoreManager core) {
		this.core = core;
	}

	public void load() {
		db = Db.open(dbLogin.getUrl(), dbLogin.getUsername(), dbLogin.getPassword());
	}

	public void unload() {
		db.close();
	}

	public Db getDb() {
		return db;
	}

	public void setDbLogin(DbLogin dbLogin) {
		this.dbLogin = dbLogin;
	}

	public void resetDb() {
		db.dropTable(net.betterverse.communities.model.Community.class);
		db.dropTable(net.betterverse.communities.model.CommunityStargate.class);
	}
}
