/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.community;

/**
 *
 * @author Admin
 */
public class DbLogin {

	private String url;
	private String username = null;
	private String password = null;

	public DbLogin(String url) {
		this.url = url;
	}

	public DbLogin(String address, String username, String password) {
		this(address);
		this.username = username;
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}
}
