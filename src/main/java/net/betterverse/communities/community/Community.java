package net.betterverse.communities.community;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import net.betterverse.bukkit.BukkitUtil;

import net.betterverse.communities.Communities;
import net.betterverse.communities.util.Saveable;
import net.betterverse.communities.util.StringHelper;
import net.betterverse.communities.util.YamlFile;
import net.betterverse.spout.util.SpoutUtil;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.dynmap.markers.AreaMarker;

public class Community extends Saveable implements Listener, Runnable {

	private static final String HOME_LOCATION_NODE = "territory.home-location";
	private static final long TAX_INTERVAL = 24 * 3600000; // 24 hours
	private final Communities plugin;
	private final String name;
	private final Map<String, CommunityPlayer> members = new ConcurrentHashMap<String, CommunityPlayer>();
	private final Map<String, String> invited = new HashMap<String, String>();
	private final Map<String, Long> inviteTimes = new ConcurrentHashMap<String, Long>();
	private final List<CommunityChunk> territory = new CopyOnWriteArrayList<CommunityChunk>(); // Need to use CopyOnWriteArrayList as it's iterated over MANY times more than modified, due to the way .getChunk(WorldCoord) operates.
	private final Set<String> townChat = Collections.synchronizedSet(new HashSet<String>());
	private final Set<Material> placeableMaterials = new HashSet<Material>();
	private final Set<Material> breakableMaterials = new HashSet<Material>();
	private final Set<String> placeableEditors = new HashSet<String>();
	private final Set<String> breakableEditors = new HashSet<String>();
	private final long inviteExpiry;
	private World world;
	private double balance;
	private String description;
	private int communityTask;
	private long expireTime;
	private boolean paymentExempt;
	private double playerTax;
	private String color = "#CC0000";
	private String mayor;
	private AreaMarker mapMarker;
	private Location homeLocation;

	public Community(Communities plugin, String name) {
		super(new YamlFile(plugin, new File(plugin.getDataFolder() + File.separator + "communities", name + ".yml"), name));

		this.plugin = plugin;
		this.name = name;
		description = "&aDefault town description. Change me with &e/coms description (text)&a!";
		communityTask = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, this, 20, 20);
		inviteExpiry = plugin.config().getInviteExpiryMillis();
		expireTime = System.currentTimeMillis() + TAX_INTERVAL;

		// Add allowed blocks from configuration file to list of placeable materials
		for (String allowed : plugin.config().getAllowedBlocks()) {
			if (Material.getMaterial(allowed) != null) {
				placeableMaterials.add(Material.getMaterial(allowed));
			}
		}

		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	public Community(Communities plugin, String name, World world, Location location) {
		this(plugin, name);
		this.world = world;

		WorldCoord initialCoord = WorldCoord.parseWorldCoord(location);
		territory.add(new CommunityChunk(plugin, initialCoord));
		homeLocation = location;

		saveTerritory();
		saveHomeLocation();
	}

	@Override
	public void load() {
		for (String player : getStringList("members")) {
			members.put(player, plugin.fromBukkitPlayer(player));
		}
		world = plugin.getServer().getWorld(getString("territory.world"));
		mapMarker = plugin.createOrFindMarker(name, world.getName(), new double[100], new double[100]);

		// Load chunks
		String key = "territory.chunks";
		ConfigurationSection chunkSection = getSection(key);
		if (getSection(key) != null) {
			for (String id : chunkSection.getKeys(false)) {
				WorldCoord coord = new WorldCoord(world.getName(), getInt(key + "." + id + ".x"), getInt(key + "." + id + ".z"));
				CommunityChunk add = new CommunityChunk(plugin, coord);
				add.load(this, chunkSection.getConfigurationSection(id));

				// Set the chunk's group if it has one
				String group = getString(key + "." + id + ".group");
				if (group != null && !group.isEmpty()) {
					CommunityChunkGroup ccg = plugin.getChunkGroup(group, this);
					if(ccg == null) {
						plugin.createChunkGroup(group, this);
						ccg = plugin.getChunkGroup(group, this);
					}
					add.setGroup(ccg);
				}

				territory.add(add);
			}
		}

		if (isSet(HOME_LOCATION_NODE)) {
			homeLocation = getLocation(HOME_LOCATION_NODE);
			homeLocation.setWorld(world);
		} else {
			plugin.log(Level.WARNING, String.format("The community '%s' does not have a homeLocation set.", name));
		}

		mayor = getString("mayor");
		// Workaround for towns created before mayors were saved to the community file
		if (mayor.isEmpty()) {
			for (CommunityPlayer member : members.values()) {
				if (member.isMayor()) {
					setMayor(member);
				}
			}
		}

		balance = getDouble("balance");
		description = getString("description", description);
		paymentExempt = getBoolean("payment-exempt");
		playerTax = getDouble("player-tax");
		color = getString("map-color", color);

		// Load placeable blocks
		key = "blocks.place";
		if (!getStringList(key).isEmpty()) {
			for (String id : getStringList(key)) {
				placeableMaterials.add(Material.getMaterial(id));
			}
		}

		// Load breakable blocks
		key = "blocks.break";
		if (!getStringList(key).isEmpty()) {
			for (String id : getStringList(key)) {
				breakableMaterials.add(Material.getMaterial(id));
			}
		}
	}

	@Override
	public void run() {
		// Check if it is time for tax collection
		if (!paymentExempt && System.currentTimeMillis() >= expireTime) {
			collectTaxes();
			expireTime = System.currentTimeMillis() + TAX_INTERVAL;
		}

		// Check for invites expiring
		for (Entry<String, Long> entry : inviteTimes.entrySet()) {
			// Cancel invitations if the time has expired
			if (entry.getValue() + inviteExpiry <= System.currentTimeMillis()) {
				String player = entry.getKey();
				acceptOrDeclineInvite(player, false);
				// Notify the player if online
				if (plugin.getServer().getPlayer(player) != null) {
					plugin.getServer().getPlayer(player).sendMessage(ChatColor.RED + "Your invitation to the town '" + name + "' has expired!");
				}
			}
		}
	}

	@Override
	public void save() {
		setSaveOnSet(false);
		_saveAll();
		setSaveOnSet(true);
		saveFile();
	}

	private void _saveAll() {
		savePlayers();
		_saveTerritory();

		set("mayor", mayor);
		set("balance", balance);
		set("description", description);
		set("payment-exempt", paymentExempt);
		set("player-tax", playerTax);
		set("map-color", color);

		saveBreakables();
		saveHomeLocation();
		savePlaceables();
	}

	private boolean canInteract(boolean isMbr, boolean hasOwnr, boolean isOwnr, boolean hasPower) {
		if (hasPower) {
			return true;
		}
		if (isOwnr) {
			return true;
		}
		if (isMbr) {
			if (!hasOwnr) {
				return true;
			} else {
				return isOwnr;
			}
		} else {
			return false;
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockBreak(BlockBreakEvent event) {
		//event.getPlayer().sendMessage("You broke a block!");
		if (!event.isCancelled() && isLocationWithinBorder(event.getBlock().getLocation())) {
			//event.getPlayer().sendMessage("Not canceled, in protected area!");
			// Only allow members to break blocks unless the block is allowed to be broken
			Chunk chunk = event.getBlock().getChunk();
			CommunityChunk ochunk = getOwnedChunk(new WorldCoord(chunk.getWorld().getName(), chunk.getX(), chunk.getZ()));
			boolean isMbr = isMember(event.getPlayer().getName());
			boolean hasOwnr = ochunk.hasOwner();
			boolean isOwnr = hasOwnr ? ochunk.getOwner().equals(event.getPlayer().getName()) : false;
			boolean hasPower = this.getMayor().equals(event.getPlayer().getName());
			//event.getPlayer().sendMessage(isMbr+" "+hasOwnr+" "+isOwnr+" "+hasPower);
			//event.getPlayer().sendMessage("GoGo");
			if (!canInteract(isMbr, hasOwnr, isOwnr, hasPower) && !breakableMaterials.contains(event.getBlock().getType())) {
				//event.getPlayer().sendMessage("Meh");
				event.setCancelled(true);
				event.getPlayer().sendMessage(ChatColor.RED + "You cannot break that block in this town.");
				if (hasOwnr) {
					event.getPlayer().sendMessage(ChatColor.RED + "This chunk is owned by " + ChatColor.YELLOW + ochunk.getOwner() + ChatColor.RED + "!");
				}
			}
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		if (!event.isCancelled() && isLocationWithinBorder(event.getBlockPlaced().getLocation())) {
			// Only allow members to place blocks unless the block is allowed to be placed
			Chunk chunk = event.getBlock().getChunk();
			CommunityChunk ochunk = getOwnedChunk(new WorldCoord(chunk.getWorld().getName(), chunk.getX(), chunk.getZ()));
			boolean isMbr = isMember(event.getPlayer().getName());
			boolean hasOwnr = ochunk.hasOwner();
			boolean isOwnr = hasOwnr ? ochunk.getOwner().equals(event.getPlayer().getName()) : false;
			boolean hasPower = this.getMayor().equals(event.getPlayer().getName());
			if (!canInteract(isMbr, hasOwnr, isOwnr, hasPower) && !placeableMaterials.contains(event.getBlockPlaced().getType())) {
				event.setCancelled(true);
				event.getPlayer().sendMessage(ChatColor.RED + "You cannot place that block in this town.");
				if (hasOwnr) {
					event.getPlayer().sendMessage(ChatColor.RED + "This chunk is owned by " + ChatColor.YELLOW + ochunk.getOwner() + ChatColor.RED + "!");
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockSpread(BlockSpreadEvent event) {
		if (!event.isCancelled() && event.getSource().getType() == Material.FIRE) {
			// Block fire from spreading from outside the border and within the border
			if (isLocationWithinBorder(event.getBlock().getLocation()) || isLocationWithinBorder(event.getSource().getLocation())) {
				if (!getOwnedChunk(WorldCoord.parseWorldCoord(event.getBlock())).canFireSpread()) {
					event.setCancelled(true);
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		// Make sure the event is within this community's territory
		if (!event.isCancelled() && isLocationWithinBorder(event.getLocation())) {
			// Cancel the spawn if the entity type is not allowed to spawn in the chunk
			if (!getOwnedChunk(WorldCoord.parseWorldCoord(event.getLocation())).canMobSpawn(event.getEntityType())) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		// Damage must be from PvP
		if (!event.isCancelled() && event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
			// The damaged player must be inside the community's territory to continue
			Player damaged = (Player) event.getEntity();
			if (isLocationWithinBorder(damaged.getLocation())) {
				// Cancel the damage PvP is not allowed in the chunk
				if (!getOwnedChunk(WorldCoord.parseWorldCoord(damaged)).isPvPAllowed()) {
					event.setCancelled(true);
					((Player) event.getDamager()).sendMessage(ChatColor.RED + "PvP is not allowed in this chunk!");
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onExplosionPrime(ExplosionPrimeEvent event) {
		if (!event.isCancelled() && isLocationWithinBorder(event.getEntity().getLocation())) {
			// Cancel creeper and TNT explosions if the configuration setting does not allow explosions
			if (!getOwnedChunk(WorldCoord.parseWorldCoord(event.getEntity())).areExplosionsAllowed()) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPaintingBreak(HangingBreakByEntityEvent event) {
		if(event.getRemover() == null) {
			System.out.println("Null remover, returning!");
		}
		if (!event.isCancelled() && isLocationWithinBorder(event.getEntity().getLocation())) {
			if (event.getRemover() instanceof Player) {
				String owner = getOwnedChunk(WorldCoord.parseWorldCoord(event.getEntity())).getOwner();
				try{
				if (!owner.equalsIgnoreCase(((Player) event.getRemover()).getName())) {
					event.setCancelled(true);
				}} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	@EventHandler
	public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
		// Do not allow non-members to place bucket contents
		if (!event.isCancelled() && isLocationWithinBorder(event.getBlockClicked().getLocation()) && !isMember(event.getPlayer().getName())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		if (members.containsKey(player.getName())) {
			if (townChat.contains(player.getName())) {
				event.setCancelled(true);
				// Send message to all members
				broadcast(plugin.config().chatPrefix(name, player.getDisplayName()) + event.getMessage());
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerInteract(PlayerInteractEvent event) {
		try{
		if (!event.isCancelled()) {
			Player player = event.getPlayer();
			Block clicked = event.getClickedBlock();
			Material material = clicked.getType();

			if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
				// Player is in an editor trying to add blocks by left clicking
				if (breakableEditors.contains(player.getName())) {
					if (!breakableMaterials.contains(material)) {
						breakableMaterials.add(material);
						player.sendMessage(ChatColor.GREEN + "Added " + ChatColor.YELLOW + material.name() + ChatColor.GREEN + " to the town's list of breakable blocks.");
						// Save the breakable materials
						saveBreakables();
					} else {
						player.sendMessage(ChatColor.RED + "That block has already been flagged as breakable.");
					}
				} else if (placeableEditors.contains(player.getName())) {
					if (!placeableMaterials.contains(material)) {
						placeableMaterials.add(material);
						player.sendMessage(ChatColor.GREEN + "Added " + ChatColor.YELLOW + material.name() + ChatColor.GREEN + " to the town's list of placeable blocks.");
						// Save the placeable materials
						savePlaceables();
					} else {
						player.sendMessage(ChatColor.RED + "That block has already been flagged as placeable.");
					}
				}
			} else if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (breakableEditors.contains(player.getName())) {
					// Player is in breakable block editor trying to remove blocks by right clicking
					if (breakableMaterials.contains(material)) {
						breakableMaterials.remove(material);
						player.sendMessage(ChatColor.GREEN + "Removed " + ChatColor.YELLOW + material.name() + ChatColor.GREEN + " from the town's list of breakable blocks.");
						// Save the breakable materials
						saveBreakables();
					} else {
						player.sendMessage(ChatColor.RED + "That block is not flagged as breakable.");
					}
				} else if (placeableEditors.contains(player.getName())) {
					// Player is in placeable block editor trying to remove blocks by right clicking
					if (placeableMaterials.contains(material)) {
						placeableMaterials.remove(material);
						player.sendMessage(ChatColor.GREEN + "Removed " + ChatColor.YELLOW + material.name() + ChatColor.GREEN + " from the town's list of placeable blocks.");
						// Save the placeable materials
						savePlaceables();
					} else {
						player.sendMessage(ChatColor.RED + "That block is not flagged as placeable.");
					}
				} else {
					// Player is trying to interact with terrain normally
					if (isLocationWithinBorder(clicked.getLocation())) {
						if (!plugin.config().canInteractWith(material)) {
							event.setCancelled(true);
							event.getPlayer().sendMessage(ChatColor.RED + "You cannot interact with that block!");
						}
					}
				}
			} else if (event.getAction() == Action.PHYSICAL && clicked.getType() == Material.CROPS) {
				if (isLocationWithinBorder(clicked.getLocation())) {
					if (!getOwnedChunk(WorldCoord.parseWorldCoord(clicked)).getOwner().equalsIgnoreCase(player.getName())) {
						event.getPlayer().sendMessage(ChatColor.RED + "You cannot interact with that block!");
						event.setCancelled(true);
					}
				}
			}
		}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		// Remove the player from block editors if necessary
		String player = event.getPlayer().getName();
		if (breakableEditors.contains(player)) {
			breakableEditors.remove(player);
		}

		if (placeableEditors.contains(player)) {
			placeableEditors.remove(player);
		}
	}

	public boolean acceptOrDeclineInvite(String player, boolean accept) {
		for (String match : invited.keySet()) {
			List<Player> matches = plugin.getServer().matchPlayer(match);
			if (matches.size() > 0) {
				if (matches.get(0).getName().equalsIgnoreCase(player)) {
					if (accept) {
						addPlayer(player, plugin.fromBukkitPlayer(player));
					}
					invited.remove(player);
					inviteTimes.remove(player);

					// Remove invitations from any other communities
					for (Community community : plugin.getCommunities()) {
						if (!community.equals(this)) {
							community.acceptOrDeclineInvite(player, false);
						}
					}
					return true;
				}
			}
		}

		return false;
	}

	public boolean addInvite(String player, String inviter) {
		if (invited.containsKey(player)) {
			return false;
		} else {
			invited.put(player, inviter);
			inviteTimes.put(player, System.currentTimeMillis());

			// Notify player if online
			Player invite = plugin.getServer().getPlayer(player);
			if (invite != null) {
				invite.sendMessage(ChatColor.GREEN + "You have been invited to join " + ChatColor.YELLOW + name + ChatColor.GREEN + " by " + ChatColor.YELLOW + invited.get(player) + ChatColor.GREEN + "! Enter " + ChatColor.YELLOW
						+ "/communities accept " + name + ChatColor.GREEN + " to accept the invitation and join the town.");
			}
			return true;
		}
	}

	public void addPlayer(String playerName, CommunityPlayer player) {
		members.put(playerName, player);
		savePlayers();
	}

	public void addToTerritory(WorldCoord coord) {
		CommunityChunk communityChunk = new CommunityChunk(plugin, coord);
		territory.add(communityChunk);
		saveTerritory();
	}

	public void deposit(double amount) {
		changeBalance(amount);
	}

	public String getBalance() {
		return plugin.economy().format(balance);
	}

	public String getDescription(WorldCoord coord) {
		CommunityChunkGroup group = getOwnedChunk(coord).getGroup();
		if (group != null) {
			// Use group's description if the chunk is part of a group
			return group.getDescription();
		} else {
			return StringHelper.parseColors(description);
		}
	}

	public String getMayor() {
		return mayor != null ? mayor : "None";
	}

	public String getName() {
		return name;
	}

	public CommunityChunk getOwnedChunk(WorldCoord coord) {
		for (CommunityChunk owned : territory) {
			if (owned.equals(coord)) {
				return owned;
			}
		}

		return null;
	}

	public int getPopulation() {
		return members.size();
	}

	public int getSize() {
		return territory.size();
	}

	public boolean isPlayerInBlockEditor(String player) {
		return breakableEditors.contains(player) || placeableEditors.contains(player);
	}

	public boolean isLocationWithinBorder(Location loc) {
		return ownsChunk(WorldCoord.parseWorldCoord(loc));
	}

	public boolean isMember(String player) {
		return members.containsKey(player);
	}

	// This method should not be listened to, but rather linked to the main class listener
	public void onJoin(PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		if (invited.containsKey(player.getName())) {
			player.sendMessage(ChatColor.GREEN + "You have been invited to join " + ChatColor.YELLOW + name + ChatColor.GREEN + " by " + ChatColor.YELLOW + invited.get(player.getName()) + ChatColor.GREEN + "! Enter " + ChatColor.YELLOW
					+ "/communities accept " + name + ChatColor.GREEN + " to accept the invitation and join the town.");
		}

		// Greet the player if SpoutCraft is enabled and it logs in within the town's boundary
		if (isLocationWithinBorder(player.getLocation()) && plugin.isSpoutEnabled()) {
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				@Override
				public void run() {
					// Workaround for issue with SpoutCraft not loading before this method is called
					if (SpoutUtil.isSpoutCraftEnabled(plugin, player)) {
						SpoutUtil.onPlayerEnterTerritory(Community.this, plugin, player);
					}
				}
			}, 100);
		}
	}

	public void onPlayerExitTerritory(Player player) {
		if (plugin.isSpoutEnabled() && SpoutUtil.isSpoutCraftEnabled(plugin, player)) {
			SpoutUtil.clearWidgets(plugin, player);
		} else {
			player.sendMessage(ChatColor.GREEN + "You have exited " + ChatColor.YELLOW + name + ChatColor.GREEN + ".");
		}
	}

	public boolean ownsChunk(WorldCoord coord) {
		return getOwnedChunk(coord) != null;
	}

	public void remove() {
		// Remove and save players
		for (String player : members.keySet()) {
			members.get(player).leaveCommunity();
			// Remove widgets from screen if member is online and using SpoutCraft
			Player check = plugin.getServer().getPlayer(player);
			if (check != null) {
				SpoutUtil.clearWidgets(plugin, check);
			}
		}

		territory.clear();
		members.clear();
		townChat.clear();
		invited.clear();
		breakableMaterials.clear();
		placeableMaterials.clear();
		breakableEditors.clear();
		placeableEditors.clear();
		balance = 0;
		HandlerList.unregisterAll(this);
		plugin.getServer().getScheduler().cancelTask(communityTask);

		// Remove from dynmap
		if(mapMarker != null) {
			mapMarker.deleteMarker();
		}
	}

	public void removePlayer(String player) {
		members.get(player).leaveCommunity();
		members.remove(player);
		savePlayers();

		// Update map description
		mapMarker.setDescription(getDynmapDescription());
	}

	public void render() {
		// Render community territory using dynmap
		// TODO: Render a static square for now
		mapMarker.setCornerLocation(0, 0, 0);
		mapMarker.setCornerLocation(1, 100, 0);
		mapMarker.setCornerLocation(2, 100, 100);
		mapMarker.setCornerLocation(3, 0, 100);

		int hex = Integer.parseInt(color.substring(1), 16);
		mapMarker.setLineStyle(2, 10, hex);
		mapMarker.setFillStyle(0.5, hex);

		mapMarker.setDescription(getDynmapDescription());
	}

	public void setDescription(String description) {
		this.description = description;
		set("description", description);

		// Update map description
		mapMarker.setDescription(getDynmapDescription());
	}

	public void setGroup(WorldCoord coord, CommunityChunkGroup group) {
		getOwnedChunk(coord).setGroup(group);
		saveTerritory();
	}

	public void setHomeLocation(Location homeLocation) {
		this.homeLocation = homeLocation;
		saveHomeLocation();
	}

	public boolean hasHomeLocation() {
		return homeLocation != null;
	}

	public void setMapColor(String color) {
		this.color = color;
		set("map-color", color);

		// Update map
		render();
	}

	public void setMayor(CommunityPlayer player) {
		mayor = player.getName();
		player.setRank(CommunityRank.MAYOR);
		set("mayor", player.getName());

		// Update map description
		mapMarker.setDescription(getDynmapDescription());
	}

	public void setPlayerTax(double playerTax) {
		this.playerTax = playerTax;
		set("player-tax", playerTax);
	}

	public void teleportHome(Player player) {
		if (hasHomeLocation()) {
			player.teleport(homeLocation);
		}
	}

	public boolean toggleBreakEditor(String player) {
		if (breakableEditors.contains(player)) {
			breakableEditors.remove(player);
			return false;
		} else {
			breakableEditors.add(player);
			return true;
		}
	}

	public boolean toggleExemptionStatus() {
		paymentExempt = !paymentExempt;
		set("payment-exempt", paymentExempt);
		return paymentExempt;
	}

	public boolean togglePlacementEditor(String player) {
		if (placeableEditors.contains(player)) {
			placeableEditors.remove(player);
			return false;
		} else {
			placeableEditors.add(player);
			return true;
		}
	}

	public boolean toggleTownChat(String player) {
		if (townChat.contains(player)) {
			townChat.remove(player);
			return false;
		} else {
			townChat.add(player);
			return true;
		}
	}

	@Deprecated
	public void unclaimChunk(CommunityChunk chunk) {
		unclaimChunk(new WorldCoord(world.getName(), chunk.getX(), chunk.getZ()));
	}

	public void unclaimChunk(WorldCoord coord) {
		if (ownsChunk(coord)) {
			territory.remove(getOwnedChunk(coord));
			saveTerritory();
		}
	}

	public boolean withdraw(double amount) {
		if (balance > amount) {
			changeBalance(-amount);
			return true;
		}

		return false;
	}

	public boolean attemptToBuyChunk() {
		return paymentExempt ? true : withdraw(plugin.config().getChunkCost(territory.size()));
	}

	private void broadcast(String message) {
		for (Player player : getOnlineMembersAsPlayers()) {
			player.sendMessage(message);
		}
	}

	public Set<Player> getOnlineMembersAsPlayers() {
		return BukkitUtil.getBukkitPlayersByName(plugin.getServer(), members.keySet());
	}

	private void changeBalance(double amount) {
		balance += amount;
		set("balance", balance);
	}

	private void collectTaxes() {
		broadcast(ChatColor.GREEN + "Collecting taxes from your town...");

		// Collect taxes from community members
		for (String member : members.keySet()) {
			if(members.get(member).isMayor()) {
				continue;
			}
			if (plugin.economy().getBalance(member) >= playerTax) {
				plugin.economy().withdrawPlayer(member, playerTax);
			} else {
				// Remove membership from any player that does not have enough to pay taxes
				Player remove = plugin.getServer().getPlayer(member);
				if (remove != null) {
					removePlayer(member);
					remove.sendMessage(ChatColor.RED + "You have been kicked from your town for not being able to pay taxes. You will need at least " + plugin.economy().format(playerTax) + " per 24 hours to stay a member.");
				}
			}
		}

		// Collect chunk taxes from this community's balance
		if (territory.isEmpty()) {
			broadcast(ChatColor.GREEN + "Your town has no chunks to tax!");
			return;
		}

		withdraw(plugin.config().getTax(territory.size()));
		// Remove chunks if the community is in debt
		while (balance <= 0) {
			for (int i = territory.size() - 1; i >= 0; i--) {
				deposit(plugin.config().getChunkCost(territory.size()));
				unclaimChunk(territory.get(i));
			}

			if (territory.isEmpty()) {
				balance = 0;
				set("balance", balance);
				broadcast(ChatColor.RED + "Your town has lost all of its chunks because it could not pay its taxes!");
				set("territory", null);
				return;
			}
		}

		saveTerritory();

		broadcast(ChatColor.GREEN + "Your town has been taxed.");
	}

	private String getDynmapDescription() {
		StringBuilder builder = new StringBuilder("<div class=\"regioninfo\">");

		builder.append("</br>Name: " + StringHelper.stripColors(name));
		builder.append("</br>Description: " + StringHelper.stripColors(description));
		builder.append("</br>Mayor: " + getMayor());

		// Alphabetize assistants
		List<String> assistants = new ArrayList<String>();
		for (CommunityPlayer member : members.values()) {
			if (member.getRank() == CommunityRank.ASSISTANT) {
				assistants.add(member.getName());
			}
		}
		// Only append assistants if at least one was found
		if (!assistants.isEmpty()) {
			builder.append("</br>Assistants: ");
			Collections.sort(assistants);

			// Only show three assistants maximum
			int max = 3;
			for (int i = 0; i < max; i++) {
				// Verify that the element exists
				if (i <= assistants.size() - 1) {
					builder.append(assistants.get(i));
					// Append comma only if not last element of list
					if (i != max - 1) {
						builder.append(", ");
					}
				}
			}
		}

		builder.append("</br>Members: " + getPopulation());
		builder.append("</div>");

		return builder.toString();
	}

	private void saveBreakables() {
		set("blocks.break", null); //TODO: Remove - redundant?
		List<String> breakable = new ArrayList<String>();
		for (Material material : breakableMaterials) {
			breakable.add(material.name());
		}
		set("blocks.break", breakable);
	}

	private void saveHomeLocation() {
		setLocation(HOME_LOCATION_NODE, homeLocation);
	}

	private void savePlaceables() {
		set("blocks.place", null); //TODO: Remove - redundant?
		List<String> placeable = new ArrayList<String>();
		for (Material material : placeableMaterials) {
			placeable.add(material.name());
		}
		set("blocks.place", placeable);
	}

	private void savePlayers() {
		set("members", null); //TODO: Remove - redundant?
		set("members", members.keySet().toArray());
	}

	public void saveTerritory() {
		setSaveOnSet(false);
		_saveTerritory();
		setSaveOnSet(true);
		saveFile();
	}

	private void _saveTerritory() {
		set("territory.world", world.getName());
		set("territory.chunks", null);
		for (int i = 0; i < territory.size(); i++) {
			CommunityChunk chunk = territory.get(i);
			set("territory.chunks." + i + ".x", chunk.getX());
			set("territory.chunks." + i + ".z", chunk.getZ());
			if (chunk.hasGroup()) {
				set("territory.chunks." + i + ".group", chunk.getGroup().getName());
			}

			// Save the chunk
			chunk.save(this, getSection("territory.chunks." + i));
		}
	}
}