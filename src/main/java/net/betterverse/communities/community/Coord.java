/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.community;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

/**
 *
 * @author Admin
 */
public class Coord {

	protected static int CELL_SIZE = 16;
	protected int x;
	protected int z;

	public Coord(int x, int z) {
		this.x = x;
		this.z = z;
	}

	public Coord(Coord coord) {
		this.x = coord.getX();
		this.z = coord.getZ();
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public Coord add(int xOffset, int zOffset) {
		return new Coord(getX() + xOffset, getZ() + zOffset);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(x)
				.append(z)
				.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Coord) {
			Coord that = (Coord) obj;
			return new EqualsBuilder()
					.append(this.x, that.x)
					.append(this.z, that.z)
					.isEquals();
		} else {
			return false;
		}
	}

	/**
	 * Convert regular grid coordinates to their grid cell's counterparts.
	 *
	 * @param x
	 * @param z
	 * @return a new instance of Coord.
	 *
	 */
	public static Coord parseCoord(int x, int z) {
		int xresult = x / CELL_SIZE;
		int zresult = z / CELL_SIZE;
		boolean xneedfix = x % CELL_SIZE != 0;
		boolean zneedfix = z % CELL_SIZE != 0;
		return new Coord(xresult - (x < 0 && xneedfix ? 1 : 0), zresult - (z < 0 && zneedfix ? 1 : 0));
	}

	public static Coord parseCoord(Entity entity) {
		return parseCoord(entity.getLocation());
	}

	public static Coord parseCoord(Location loc) {
		return parseCoord(loc.getBlockX(), loc.getBlockZ());
	}

	public static Coord parseCoord(Block block) {
		return parseCoord(block.getX(), block.getZ());
	}

	@Override
	public String toString() {

		return getX() + "," + getZ();
	}

	public Vector getMinVector() {
		int x = getX() * CELL_SIZE;
		int z = getZ() * CELL_SIZE;
		return new Vector(x, 0, z);
	}

	public Vector getMaxVector(World world) {
		int x = getX() * CELL_SIZE + CELL_SIZE - 1;
		int z = getZ() * CELL_SIZE + CELL_SIZE - 1;
		return new Vector(x, world.getMaxHeight() - 1, z);
	}
}
