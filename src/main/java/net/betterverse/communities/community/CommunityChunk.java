package net.betterverse.communities.community;

import java.util.HashSet;
import java.util.Set;
import net.betterverse.communities.Communities;
import org.bukkit.Chunk;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;

public class CommunityChunk {

	private static final EntityType[] MONSTERS = new EntityType[]{EntityType.BLAZE, EntityType.CAVE_SPIDER, EntityType.CREEPER, EntityType.ENDER_DRAGON, EntityType.ENDERMAN, EntityType.GHAST, EntityType.GIANT, EntityType.IRON_GOLEM,
		EntityType.MAGMA_CUBE, EntityType.PIG_ZOMBIE, EntityType.SILVERFISH, EntityType.SKELETON, EntityType.SLIME, EntityType.SPIDER, EntityType.ZOMBIE};
	private final Communities plugin;
	private final String world;
	private final int x;
	private final int z;
	private final Set<EntityType> canSpawn = new HashSet<EntityType>();
	private CommunityChunkGroup group;
	private String owner;
	private int onSale;
	private boolean pvp;
	private boolean explosions;
	private boolean fireSpread;
	private boolean monsters;

	public CommunityChunk(Communities plugin, WorldCoord coord) {
		this(plugin, coord.getWorldId(), coord.getX(), coord.getZ());
	}

	private CommunityChunk(Communities plugin, String world, int x, int z) {
		this.plugin = plugin;
		this.world = world;
		this.x = x;
		this.z = z;

		pvp = plugin.config().canPvP();
		explosions = plugin.config().areExplosionsAllowed();
		fireSpread = plugin.config().canFireSpread();

		// Populate spawnable mobs with initial values from configuration file
		for (EntityType mob : EntityType.values()) {
			if (plugin.config().canMobSpawn(mob)) {
				canSpawn.add(mob);
			}
		}
	}

	public boolean areExplosionsAllowed() {
		if (group == null) {
			return explosions;
		} else {
			return group.areExplosionsAllowed();
		}
	}

	public boolean canFireSpread() {
		if (group == null) {
			return fireSpread;
		} else {
			return group.canFireSpread();
		}
	}

	public boolean canMobSpawn(EntityType type) {
		if (group == null) {
			return canSpawn.contains(type);
		} else {
			return group.canMobSpawn(type);
		}
	}

	public CommunityChunkGroup getGroup() {
		return group;
	}

	public boolean hasGroup() {
		return group != null;
	}

	public int getX() {
		return x;
	}

	public int getZ() {
		return z;
	}

	public boolean isPvPAllowed() {
		if (group == null) {
			return pvp;
		} else {
			return group.isPvPAllowed();
		}
	}

	public void load(Community community, ConfigurationSection section) {
		String name = section.getString("group");
		if (name != null && !name.isEmpty()) {
			group = plugin.getChunkGroup(name, community);
			if(group == null) {
				plugin.createChunkGroup(name, community);
				group = plugin.getChunkGroup(name, community);
			}
			group.load();
		} else {
			owner = section.getString("owner");
			onSale = section.getInt("on-sale");
			pvp = section.getBoolean("pvp");
			explosions = section.getBoolean("explosions");
			fireSpread = section.getBoolean("fire-spread");
			for (String mob : section.getStringList("spawnable")) {
				EntityType entityType = EntityType.fromName(mob);
				if (entityType != null) {
					canSpawn.add(entityType);
				}
			}
		}
	}

	public void save(Community community, ConfigurationSection section) {
		if (group != null) {
			String name = group.getName();
			section.set("group", name);
			group.save();
		} else {
			section.set("on-sale", onSale);
			section.set("owner", owner);
			section.set("pvp", pvp);
			section.set("explosions", explosions);
			section.set("fire-spread", fireSpread);

			// Convert spawnables to strings
			Set<String> converted = new HashSet<String>();
			for (EntityType spawn : canSpawn) {
				converted.add(spawn.name());
			}
			section.set("spawnable", converted.toArray());
		}
	}

	public void setGroup(CommunityChunkGroup group) {
		this.group = group;
		group.addChild(this);
	}

	public boolean hasOwner() {
		if(group == null) {
			return owner != null;
		} else {
			return group.hasOwner();
		}
	}

	public String getOwner() {
		if(group == null) {
			return owner;
		} else {
			return group.getOwner();
		}
	}

	public void setOwner(String newOwner) {
		if(group == null) {
			owner = newOwner;
		} else {
			group.setOwner(newOwner);
		}
	}
	
	public boolean isOnSale() {
		if(group == null) {
			return onSale != 0;
		} else {
			return group.isOnSale();
		}
	}
	
	public int getSalePrice() {
		if(group == null) {
			return onSale;
		} else {
			return group.getSalePrice();
		}
	}
	
	public void setOnSale(int onSale) {
		if(group == null) {
			this.onSale = onSale;
		} else {
			group.setOnSale(onSale);
		}
	}

	public boolean toggleMonsters() {
		if (monsters) {
			// Remove all monsters from the spawn list
			for (EntityType monster : MONSTERS) {
				canSpawn.remove(monster);
			}
		} else {
			// Add all monsters to the spawn list
			for (EntityType monster : MONSTERS) {
				canSpawn.add(monster);
			}
		}

		return monsters = !monsters;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Chunk) {
			Chunk chunk = (Chunk) obj;
			return x == chunk.getX() && z == chunk.getZ() && world.equals(chunk.getWorld().getName());
		} else if (obj instanceof CommunityChunk) {
			CommunityChunk chunk = (CommunityChunk) obj;
			return x == chunk.x && z == chunk.z && world.equals(chunk.world);
		} else if (obj instanceof WorldCoord) {
			WorldCoord coord = (WorldCoord) obj;
			return x == coord.getX() && z == coord.getZ() && world.equals(coord.getWorldId());
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return String.format("CommunityChunk(%d, %d)", x, z);
	}
}
