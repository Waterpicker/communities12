package net.betterverse.communities.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Paginator {

	private static final int LINES_PER_PAGE = 8;
	private final List<String> lines = new ArrayList<String>();
	private String header;
	private String description;
	private int maxPages;

	public void addLine(String line) {
		lines.add(line);
	}

	public Paginator header(String header) {
		this.header = StringHelper.parseColors(header);
		return this;
	}

	public Paginator description(String description) {
		this.description = StringHelper.parseColors(description);
		return this;
	}

	public void sendPage(CommandSender sender, int page) {
		maxPages = (int) ((lines.size() / LINES_PER_PAGE == 0) ? 1 : Math.ceil((double) lines.size() / LINES_PER_PAGE));
		if (page < 0 || page > maxPages) {
			sender.sendMessage(ChatColor.RED + "'" + page + "' is not a valid page. The maximum amount of pages is " + maxPages + ".");
			return;
		}

		int startIndex = LINES_PER_PAGE * page - LINES_PER_PAGE;
		int endIndex = page * LINES_PER_PAGE;

		sender.sendMessage(header.replace("<page>", String.valueOf(page)).replace("<max-pages>", String.valueOf(maxPages)));
		if (!description.isEmpty()) {
			sender.sendMessage(description);
		}

		if (lines.size() < endIndex) {
			endIndex = lines.size();
		}

		for (String line : lines.subList(startIndex, endIndex)) {
			sender.sendMessage(StringHelper.parseColors(line));
		}
	}
}
