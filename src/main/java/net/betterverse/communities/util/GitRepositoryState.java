/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.util;

import java.util.Properties;

/**
 *
 * @author Admin
 */
public class GitRepositoryState {

	public final String branch;                  // =${git.branch}
	public final String commitId;                // =${git.commit.id}
	public final String commitIdAbbrev;          // =${git.commit.id.abbrev}
	public final String buildUserName;           // =${git.build.user.name}
	public final String buildUserEmail;          // =${git.build.user.email}
	public final String buildTime;               // =${git.build.time}
	public final String commitUserName;          // =${git.commit.user.name}
	public final String commitUserEmail;         // =${git.commit.user.email}
	public final String commitMessageShort;      // =${git.commit.message.short}
	public final String commitMessageFull;       // =${git.commit.message.full}
	public final String commitTime;              // =${git.commit.time}

	public GitRepositoryState(Properties properties) {
		this.branch = properties.getProperty("git.branch");
		this.commitId = properties.getProperty("git.commit.id");
		this.commitIdAbbrev = properties.getProperty("git.commit.id.abbrev");
		this.buildUserName = properties.getProperty("git.build.user.name");
		this.buildUserEmail = properties.getProperty("git.build.user.email");
		this.buildTime = properties.getProperty("git.build.time");
		this.commitUserName = properties.getProperty("git.commit.user.name");
		this.commitUserEmail = properties.getProperty("git.commit.user.email");
		this.commitMessageShort = properties.getProperty("git.commit.message.short");
		this.commitMessageFull = properties.getProperty("git.commit.message.full");
		this.commitTime = properties.getProperty("git.commit.time");
	}

	public String getCommitGihubUrl(String githubRepositoryUrl) {
		String url = githubRepositoryUrl;
		url += (githubRepositoryUrl.endsWith("/") ? "" : "/");
		url += "commit/";
		url += commitIdAbbrev;
		return url;
	}
}
