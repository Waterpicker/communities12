package net.betterverse.communities.util;

import java.util.List;

import org.bukkit.configuration.ConfigurationSection;

import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public abstract class Saveable {

	private final YamlFile file;

	public Saveable(YamlFile file) {
		this.file = file;
	}

	public abstract void load();

	public abstract void save();

	public boolean getBoolean(String key) {
		return (Boolean) file.getValue(key, false);
	}

	public double getDouble(String key) {
		return (Double) file.getValue(key, 0D);
	}

	public int getInt(String key) {
		return getInt(key, 0);
	}

	public int getInt(String key, int def) {
		return (Integer) file.getValue(key, def);
	}

	public float getFloat(String key) {
		Object value = file.getValue(key, 0F);
		if (value instanceof Float) {
			return (Float) value;
		} else if (value instanceof Number) {
			Number number = (Number) value;
			return number.floatValue();
		}
		return 0F;
	}

	public Location getLocation(String key) {
		String worldName = getString(key + ".world");
		World world = null;
		if (worldName != null && !worldName.isEmpty()) {
			world = Bukkit.getWorld(worldName);
		}
		double x = getDouble(key + ".x");
		double y = getDouble(key + ".y");
		double z = getDouble(key + ".z");
		float yaw = getFloat(key + ".yaw");
		float pitch = getFloat(key + ".pitch");

		return new Location(world, x, y, z, yaw, pitch);
	}

	public void setLocation(String path, Location loc) {
		set(path + ".world", loc.getWorld().getName());
		set(path + ".x", loc.getX());
		set(path + ".y", loc.getY());
		set(path + ".z", loc.getZ());
		set(path + ".yaw", loc.getYaw());
		set(path + ".pitch", loc.getPitch());
	}

	public ConfigurationSection getSection(String key) {
		return file.getSection(key);
	}

	public String getString(String key) {
		return getString(key, "");
	}

	public String getString(String key, String def) {
		return String.valueOf(file.getValue(key, def));
	}

	@SuppressWarnings("unchecked")
	public List<String> getStringList(String key) {
		return (List<String>) file.getValue(key, Lists.newArrayList());
	}

	public void set(String key, Object value) {
		file.set(key, value);
	}

	public boolean isSet(String key) {
		return file.isSet(key);
	}

	public YamlFile getFile() {
		return file;
	}

	public void setSaveOnSet(boolean saveOnSet) {
		getFile().setSaveOnSet(saveOnSet);
	}

	public void saveFile() {
		getFile().save();
	}
}
