/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities;

import net.betterverse.communities.community.DbLogin;

/**
 *
 * @author Admin
 */
public class DbAddress {

	DbBackend dbBackend;
	String address;

	public DbAddress(DbBackend dbBackend, String address) {
		this.dbBackend = dbBackend;
		this.address = address;
	}

	public String toString() {
		return String.format("jdbc:%s:%s", dbBackend, address);
	}

	public enum DbBackend {

		H2,
		HSQLDB,
		Derby,
		MySQL,
		PostgresSQL;

		@Override
		public String toString() {
			return name().toLowerCase();
		}
	}
}
