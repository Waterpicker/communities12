/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.listener;

import java.util.LinkedHashSet;
import net.betterverse.bukkit.BukkitUtil;
import net.betterverse.communities.Communities;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.WorldCoord;
import net.betterverse.communities.event.CommunityClaimChunkEvent;
import net.betterverse.communities.event.CommunityUnclaimChunkEvent;
import net.betterverse.communities.task.ChunkSelection;
import net.betterverse.communities.task.SingleChunkSelection;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author Admin
 */
public class CommunitiesChunkListener implements Listener {

	Communities plugin;

	public CommunitiesChunkListener(Communities plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onClaimChunk(CommunityClaimChunkEvent event) {
		if (event.getClaimTask() instanceof SingleChunkSelection) {
			SingleChunkSelection selection = (SingleChunkSelection) event.getClaimTask();
			WorldCoord coord = selection.getFirstSelection();

			try {
				checkIfClaimIsOk(event.getCommunity(), coord);
			} catch (CommandException e) {
				BukkitUtil.sendTo(event.getSender(), ChatColor.RED + "%s", e.getMessage());
				event.setCancelled(true);
			}
		} else if (event.getClaimTask() instanceof ChunkSelection) {
			ChunkSelection selection = (ChunkSelection) event.getClaimTask();
			for (WorldCoord coord : new LinkedHashSet<WorldCoord>(selection.getSelection())) {
				try {
					checkIfClaimIsOk(event.getCommunity(), coord);
				} catch (CommandException e) {
					selection.removeFromSelection(coord);
				}
			}
		}
	}

	public void checkIfClaimIsOk(Community community, WorldCoord coord) throws CommandException {
		if (plugin.isChunkOwned(coord)) {
			//TODO: Assumes the sender of the chunk claim is the same as the community affected by the event.
			if (community.ownsChunk(coord)) {
				throw new CommandException("Your town already owns this chunk.");
			} else {
				throw new CommandException("Another town already owns this chunk.");
			}
		}
	}

	@EventHandler
	public void onUnclaimChunk(CommunityUnclaimChunkEvent event) {
		if (event.getUnclaimTask() instanceof SingleChunkSelection) {
			SingleChunkSelection selection = (SingleChunkSelection) event.getUnclaimTask();
			WorldCoord coord = selection.getFirstSelection();

			try {
				checkIfUnclaimIsOk(event.getCommunity(), coord);
			} catch (CommandException e) {
				BukkitUtil.sendTo(event.getSender(), ChatColor.RED + "%s", e.getMessage());
				event.setCancelled(true);
			}
		}
	}

	public void checkIfUnclaimIsOk(Community community, WorldCoord coord) throws CommandException {
		if (!plugin.isChunkOwned(coord)) {
			throw new CommandException("This chunk isn't owned.");
		}
		//TODO: Assumes the sender of the chunk claim is the same as the community affected by the event.
		if (!community.ownsChunk(coord)) {
			throw new CommandException("You do not own this chunk!");
		}
	}
}
