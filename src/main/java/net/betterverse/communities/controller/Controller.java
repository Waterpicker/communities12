/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.controller;

import com.iciql.Db;
import net.betterverse.communities.community.CoreManager;

/**
 *
 * @author Admin
 */
public abstract class Controller {

	private CoreManager core;

	public Controller(CoreManager core) {
		this.core = core;
	}

	public CoreManager getCore() {
		return core;
	}

	public Db getDb() {
		return getCore().getDbManager().getDb();
	}
}
