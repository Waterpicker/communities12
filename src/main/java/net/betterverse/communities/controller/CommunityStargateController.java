/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.controller;

import com.iciql.Db;
import com.iciql.Query;
import com.iciql.QueryWhere;
import java.util.List;
import net.betterverse.communities.community.CoreManager;
import net.betterverse.communities.model.Community;
import net.betterverse.communities.model.CommunityStargate;

/**
 *
 * @author Admin
 */
public class CommunityStargateController extends Controller {

	public CommunityStargateController(CoreManager core) {
		super(core);
	}

	private QueryWhere<CommunityStargate> queryPortal(String portalName, String networkName) {
		Db db = getDb();
		CommunityStargate cs = new CommunityStargate(); // Not Thread Safe.
		return db.from(cs)
				.where(cs.portalName).is(portalName)
				.and(cs.networkName).is(networkName);
	}

	public void validate(CommunityStargate communityStargate) throws Exception {
		long count = queryPortal(communityStargate.portalName, communityStargate.networkName).selectCount();
		if (count > 0) {
			throw new Exception(String.format("A stargate with the name '%s' already exists.", communityStargate.portalName));
		}


	}

	public long register(CommunityStargate communityStargate) {
		Db db = getDb();
		return db.insertAndGetKey(communityStargate);
	}

	public void delete(CommunityStargate communityStargate) {
		Db db = getDb();
		db.delete(communityStargate);
	}

	public CommunityStargate getByPortalName(String portalName, String networkName) {
		return queryPortal(portalName, networkName).selectFirst();
	}

	public void deleteAllBelongingTo(Community community) {
		Db db = getDb();
		CommunityStargate cs = new CommunityStargate(); // Not Thread Safe.
		List<CommunityStargate> communityStargates = db.from(cs).where(cs.communityId).is(community.id).select();
		db.deleteAll(communityStargates);
	}
}
