package net.betterverse.communities.command;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import net.betterverse.communities.community.CommunityRank;

@Retention(RetentionPolicy.RUNTIME)
public @interface AnnotatedCommand {

	public String name();

	public String usage();

	public String desc();

	public String permission() default "";

	public int max() default -1;

	public int min() default 1;

	public boolean server() default false;

	public boolean community() default false;

	public CommunityRank rank() default CommunityRank.NONE;

	public boolean listInHelp() default true;
}
