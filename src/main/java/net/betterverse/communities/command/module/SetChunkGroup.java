package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityChunkGroup;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;
import net.betterverse.communities.community.WorldCoord;

@AnnotatedCommand(name = "setgroup", usage = "setgroup <name>", desc = "Set a chunk's group", permission = "communities.normal.setgroup", min = 2, max = 2, community = true, rank = CommunityRank.ASSISTANT)
public class SetChunkGroup extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = player.getCommunity();
		Player pSender = (Player) sender;
		WorldCoord coord = WorldCoord.parseWorldCoord(pSender);
		if (community.ownsChunk(coord)) {
			CommunityChunkGroup cgroup = plugin.getChunkGroup(args[1], community);
			if (cgroup == null) {
				throw new CommandException("That chunk group doesn't exist!");
			}
			community.setGroup(coord, cgroup);
			pSender.sendMessage(ChatColor.GREEN + "Added the chunk at your location to the group " + ChatColor.YELLOW + args[1] + ChatColor.GREEN + ".");
		} else {
			throw new CommandException("Your town does not own the chunk at your location.");
		}
	}
}
