package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "invite", usage = "invite <player>", desc = "Invite a player to your town", permission = "communities.normal.invite", min = 2, max = 2, community = true, rank = CommunityRank.ASSISTANT)
public class Invite extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = player.getCommunity();
		if (community.addInvite(args[1].toLowerCase(), sender.getName())) {
			sender.sendMessage(ChatColor.YELLOW + args[1] + ChatColor.GREEN + " has been invited to the town.");
		} else {
			throw new CommandException(args[1] + " has already been invited to the town.");
		}
	}
}
