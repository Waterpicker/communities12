package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityPlayer;

@AnnotatedCommand(name = "deposit", usage = "deposit <amount>", desc = "Deposit to the town balance", permission = "communities.normal.balance.deposit", min = 2, max = 2, community = true)
public class Deposit extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		double deposit = Double.parseDouble(args[1]);
		if (deposit > 0) {
			if (plugin.economy().getBalance(sender.getName()) > deposit) {
				plugin.economy().withdrawPlayer(sender.getName(), deposit);
				sender.sendMessage(ChatColor.GREEN + "You have deposited " + ChatColor.YELLOW + plugin.economy().format(deposit) + ChatColor.GREEN + " to the town's balance.");
				player.getCommunity().deposit(deposit);
			} else {
				throw new CommandException("You do not have enough funds to deposit to the town's balance.");
			}
		} else {
			throw new CommandException("Invalid amount.");
		}
	}
}
