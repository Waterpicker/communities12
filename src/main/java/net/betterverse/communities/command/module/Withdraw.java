package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "withdraw", usage = "withdraw <amount>", desc = "Withdraw from the town balance", permission = "communities.normal.balance.withdraw", min = 2, max = 2, community = true, rank = CommunityRank.ASSISTANT)
public class Withdraw extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		double withdraw = Double.parseDouble(args[1]);
		if (withdraw > 0) {
			if (player.getCommunity().withdraw(withdraw)) {
				plugin.economy().depositPlayer(sender.getName(), withdraw);
				sender.sendMessage(ChatColor.GREEN + "You have withdrawn " + ChatColor.YELLOW + plugin.economy().format(withdraw) + ChatColor.GREEN + " from the town's balance.");
			} else {
				throw new CommandException("The town does not have enough funds for you to withdraw that amount.");
			}
		} else {
			throw new CommandException("Invalid amount.");
		}
	}
}
