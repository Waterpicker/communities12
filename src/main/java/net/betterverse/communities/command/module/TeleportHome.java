package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityPlayer;

@AnnotatedCommand(name = "home", usage = "home", desc = "Teleport to your town's home", permission = "communities.normal.home", max = 1, community = true)
public class TeleportHome extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		if (!player.getCommunity().hasHomeLocation()) {
			throw new CommandException(String.format("Your town does not have a home location set."));
		}

		Player pSender = (Player) sender;
		double price = plugin.config().getHomePrice();
		if (plugin.economy().getBalance(pSender.getName()) >= price) {
			player.getCommunity().teleportHome(pSender);
			plugin.economy().withdrawPlayer(player.getName(), price);
			pSender.sendMessage(ChatColor.GREEN + "Teleported to your town's home location for " + ChatColor.YELLOW + plugin.economy().format(price) + ChatColor.GREEN + "!");
		} else {
			throw new CommandException("It costs " + plugin.economy().format(price) + " to teleport to your town's home.");
		}
	}

	@Override
	public boolean canExecute() {
		return plugin.config().areHomesEnabled();
	}
}
