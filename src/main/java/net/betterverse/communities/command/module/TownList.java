package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.util.Paginator;

@AnnotatedCommand(name = "list", usage = "list (page)", desc = "List towns", permission = "communities.normal.list", max = 2)
public class TownList extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		int page = 1;
		if (args.length == 2) {
			page = Integer.parseInt(args[1]);
		}

		Paginator paginator = new Paginator();
		paginator.header("                    " + ChatColor.UNDERLINE + "&6Communities" + ChatColor.RESET + " <<page>/<max-pages>>");
		paginator.description("    &7** &eName &a(&eTotal Members&a) &7**");
		for (Community community : plugin.getCommunities()) {
			paginator.addLine("&e" + community.getName() + " &a(&e" + community.getPopulation() + "&a)");
		}
		paginator.sendPage(sender, page);
	}
}
