package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.command.CommandUsageException;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "assistant", usage = "assistant <add|remove> <player>", desc = "Set town assistants", permission = "communities.normal.assistants", min = 3, max = 3, community = true, rank = CommunityRank.ASSISTANT)
public class SetAssistant extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = player.getCommunity();
		CommunityPlayer assistant = plugin.fromBukkitPlayer(args[2]);
		if (assistant != null && assistant.getCommunity().getName().equals(community.getName())) {
			Player assistantPlayer = plugin.getServer().getPlayer(args[2]);
			if (args[1].equalsIgnoreCase("add")) {
				if (!assistant.isLeader()) {
					assistant.setRank(CommunityRank.ASSISTANT);
					sender.sendMessage(ChatColor.GREEN + "You have promoted " + ChatColor.YELLOW + assistantPlayer.getName() + ChatColor.GREEN + " to assistant.");
					// Send message to new assistant if online
					if (assistantPlayer != null) {
						assistantPlayer.sendMessage(ChatColor.GREEN + "You have been promoted to assistant by " + ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + "!");
					}
				} else {
					throw new CommandException(args[2] + " is already a leader of your town.");
				}
			} else if (args[1].equalsIgnoreCase("remove")) {
				if (assistant.isLeader() && !assistant.isMayor()) {
					assistant.setRank(CommunityRank.MEMBER);
					sender.sendMessage(ChatColor.GREEN + "You have demoted " + ChatColor.YELLOW + assistantPlayer.getName() + ChatColor.GREEN + ".");
					assistantPlayer.sendMessage(ChatColor.GREEN + "You have been demoted by " + ChatColor.YELLOW + sender.getName() + ChatColor.GREEN + "!");
				} else {
					throw new CommandException(assistantPlayer.getName() + " is not an assistant of your town.");
				}
			} else {
				throw new CommandUsageException();
			}
		} else {
			throw new CommandException(args[2] + " is not a member of your town.");
		}
	}
}
