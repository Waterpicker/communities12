package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;

@AnnotatedCommand(name = "exempt", usage = "exempt <town>", desc = "Exempt a town from payments", permission = "communities.admin.exempt", min = 2, max = 2, community = true)
public class ExemptPaying extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = plugin.getCommunity(args[1]);
		if (community != null) {
			if (community.toggleExemptionStatus()) {
				sender.sendMessage(ChatColor.YELLOW + community.getName() + ChatColor.GREEN + " is now exempt from all payments.");
			} else {
				sender.sendMessage(ChatColor.YELLOW + community.getName() + ChatColor.GREEN + " is no longer exempt from all payments.");
			}
		} else {
			throw new CommandException("'" + args[1] + "' is not a valid town name.");
		}
	}
}
