package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;

@AnnotatedCommand(name = "decline", usage = "decline <town>", desc = "Decline an invitation from a town", permission = "communities.normal.decline", min = 2, max = 2)
public class DeclineInvitation extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = plugin.getCommunity(args[1]);
		if (community != null) {
			if (community.acceptOrDeclineInvite(sender.getName(), false)) {
				sender.sendMessage(ChatColor.GREEN + "You have declined an invitation from " + ChatColor.YELLOW + community.getName() + ChatColor.GREEN + "!");
			} else {
				throw new CommandException("You were not invited by that town!");
			}
		} else {
			throw new CommandException("The town '" + args[1] + "' does not exist.");
		}
	}
}
