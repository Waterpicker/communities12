package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;

@AnnotatedCommand(name = "info", usage = "info", desc = "Display town info", permission = "communities.normal.info", max = 1, community = true)
public class TownInfo extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = player.getCommunity();
		sender.sendMessage(ChatColor.GREEN + "===== [ " + ChatColor.YELLOW + "Town Information" + ChatColor.GREEN + " ] =====");
		sender.sendMessage(ChatColor.GREEN + "Name: " + ChatColor.YELLOW + community.getName());
		sender.sendMessage(ChatColor.GREEN + "Mayor: " + ChatColor.YELLOW + community.getMayor());
		sender.sendMessage(ChatColor.GREEN + "Members: " + ChatColor.YELLOW + community.getPopulation());
		sender.sendMessage(ChatColor.GREEN + "Chunks claimed: " + ChatColor.YELLOW + community.getSize());
		sender.sendMessage(ChatColor.GREEN + "Current tax costs: " + ChatColor.YELLOW + plugin.economy().format(plugin.config().getTax(community.getSize())));
		sender.sendMessage(ChatColor.GREEN + "Your rank: " + ChatColor.YELLOW + player.getRank().name().toLowerCase());
	}
}
