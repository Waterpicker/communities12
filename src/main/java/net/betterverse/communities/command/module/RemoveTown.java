package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "remove", usage = "remove", desc = "Permanently remove your town", permission = "communities.normal.remove", max = 1, community = true, rank = CommunityRank.MAYOR)
public class RemoveTown extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		String name = player.getCommunity().getName();
		if (plugin.removeCommunity(name)) {
			sender.sendMessage(ChatColor.YELLOW + name + ChatColor.GREEN + " is no more!");
		} else {
			throw new CommandException("Could not remove community!");
		}
	}
}
