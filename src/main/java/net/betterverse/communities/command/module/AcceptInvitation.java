package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "accept", usage = "accept <town>", desc = "Accept an invitation from a town", permission = "communities.normal.accept", min = 2, max = 2)
public class AcceptInvitation extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		if (player.getCommunity() == null) {
			Community community = plugin.getCommunity(args[1]);
			if (community != null) {
				if (community.acceptOrDeclineInvite(sender.getName(), true)) {
					player.joinCommunity(community, CommunityRank.MEMBER);
					sender.sendMessage(ChatColor.GREEN + "You have joined " + ChatColor.YELLOW + community.getName() + ChatColor.GREEN + "!");
				} else {
					throw new CommandException("You were not invited by that town!");
				}
			} else {
				throw new CommandException("The town '" + args[1] + "' does not exist.");
			}
		} else {
			throw new CommandException("You already are part of a town. You must leave it before joining a new town.");
		}
	}
}
