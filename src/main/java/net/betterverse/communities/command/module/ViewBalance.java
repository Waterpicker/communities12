package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;

@AnnotatedCommand(name = "balance", usage = "balance", desc = "View the town balance", permission = "communities.normal.balance", max = 1, community = true)
public class ViewBalance extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = player.getCommunity();
		sender.sendMessage(ChatColor.GREEN + community.getName() + "'s Current Balance: " + ChatColor.YELLOW + community.getBalance());
	}
}
