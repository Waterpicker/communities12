/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.command.module;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityChunk;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.WorldCoord;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author ZNickq
 */
@AnnotatedCommand(name = "buychunk", usage = "buychunk", desc = "Buy the chunk you're currently in, if it's on sale!", permission = "communities.normal.buychunk", max = 1, community = false)
public class BuyChunk extends CommandModule{

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		WorldCoord coord = WorldCoord.parseWorldCoord((Player) sender);
		
		CommunityChunk chunk = player.getPlugin().getChunkOwner(coord).getOwnedChunk(coord);
		if(chunk == null) {
			sender.sendMessage(ChatColor.RED+"This chunk is not owned by a community!");
			return;
		}
		if(!chunk.isOnSale()) {
			sender.sendMessage(ChatColor.RED+"This chunk is not on sale!");
			return;
		}
		//Charge the player
		if(!player.charge(chunk.getSalePrice())) {
			sender.sendMessage(ChatColor.RED+"You do not have enough money to buy this chunk, you need "+chunk.getSalePrice()+"!");
			return;
		}
		int price = chunk.getSalePrice();
		chunk.setOnSale(0);
		chunk.setOwner(sender.getName());
		sender.sendMessage(ChatColor.GREEN+"Bought chunk for "+price+"!");
		
	}
	
}
