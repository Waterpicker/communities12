package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;
import net.betterverse.communities.event.CommunityCreateEvent;
import org.bukkit.Location;

@AnnotatedCommand(name = "create", usage = "create <name>", desc = "Create a town at your location", permission = "communities.normal.create", min = 2, max = 2)
public class CreateTown extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Player pSender = (Player) sender;

		String communityName = args[1];
		Location location = pSender.getLocation();
		CommunityCreateEvent event = new CommunityCreateEvent(pSender, communityName, location);
		plugin.getServer().getPluginManager().callEvent(event);

		if (event.isCancelled()) {
			return;
		}

		Community community = plugin.createCommunity(args[1], pSender.getWorld(), pSender.getLocation());
		community.addPlayer(pSender.getName(), player);

		// Give the community an initial balance
		double initialBalance = plugin.config().getInitialCommunityBalance();
		if (initialBalance > 0) {
			community.deposit(initialBalance);
		}

		player.joinCommunity(community, CommunityRank.MAYOR);
		pSender.sendMessage(ChatColor.YELLOW + args[1] + ChatColor.GREEN + " successfully created. You are now the mayor.");
	}
}
