package net.betterverse.communities.command.module;

import java.util.Collection;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityChunkGroup;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "listgroups", usage = "listgroups", desc = "List chunk groups", permission = "communities.normal.listgroups", min = 1, max = 2, community = true, rank = CommunityRank.MEMBER)
public class ListChunkGroups extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		int pagenumber = 0;
		Community community = player.getCommunity();
		Player pSender = (Player) sender;
		Collection<CommunityChunkGroup> cc = plugin.getChunkGroups(community);
		if (args.length == 2) {
			try {
				pagenumber = Integer.parseInt(args[1]);
			} catch (Exception ex) {
				pSender.sendMessage(ChatColor.RED + args[1] + " is not a number!");
				return;
			}
		}
		pSender.sendMessage(ChatColor.GREEN + "Your community's chunk groups, page "+pagenumber+"/"+cc.size()+":");
		int displayed = 0;
		for (CommunityChunkGroup ccn : cc) {
			if(displayed >= pagenumber * 5 && displayed < pagenumber * 5 + 5) {
				pSender.sendMessage(ChatColor.YELLOW + ccn.getName() + " owned by " + ccn.getOwner());
			}
			displayed++;
		}
	}
}
