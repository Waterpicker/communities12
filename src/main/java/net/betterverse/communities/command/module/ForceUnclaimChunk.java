package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.WorldCoord;

@AnnotatedCommand(name = "forceunclaim", usage = "forceunclaim", desc = "Force unclaim the chunk at your location", permission = "communities.admin.unclaim", max = 1)
public class ForceUnclaimChunk extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Player pSender = (Player) sender;
		WorldCoord coord = WorldCoord.parseWorldCoord(pSender);
		if (plugin.isChunkOwned(coord)) {
			Community community = plugin.getChunkOwner(coord);
			community.unclaimChunk(coord);
			pSender.sendMessage(ChatColor.GREEN + "Removed chunk from " + ChatColor.YELLOW + community.getName() + ChatColor.GREEN + ".");
		} else {
			throw new CommandException("This chunk is not owned.");
		}
	}
}
