package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;
import net.betterverse.communities.community.WorldCoord;
import net.betterverse.communities.task.UnclaimSingleTask;
import net.betterverse.communities.task.UnclaimTask;
import org.bukkit.Bukkit;

@AnnotatedCommand(name = "unclaim", usage = "unclaim", desc = "Unclaim the chunk at your location", permission = "communities.normal.unclaim", max = 1, community = true, rank = CommunityRank.ASSISTANT)
public class UnclaimChunk extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Player pSender = (Player) sender;
		Community community = player.getCommunity();

		UnclaimTask task = new UnclaimSingleTask(community, pSender.getLocation());
		task.setSender(sender);
		Bukkit.getScheduler().scheduleAsyncDelayedTask(plugin, task);
	}
}
