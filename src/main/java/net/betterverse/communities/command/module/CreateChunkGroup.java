package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;
import net.betterverse.communities.community.WorldCoord;

@AnnotatedCommand(name = "creategroup", usage = "creategroup <name>", desc = "Create a chunk group", permission = "communities.normal.creategroup", min = 2, max = 2, community = true, rank = CommunityRank.ASSISTANT)
public class CreateChunkGroup extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = player.getCommunity();
		Player pSender = (Player) sender;
		WorldCoord coord = WorldCoord.parseWorldCoord(pSender);
		if (community.ownsChunk(coord)) {
			if (plugin.getChunkGroup(args[1], community) != null) {
				throw new CommandException("This chunk group already exists!");
			}
			plugin.createChunkGroup(args[1], community);
			pSender.sendMessage(ChatColor.GREEN + "Created chunk group: "+args[1]+"!");
		} else {
			throw new CommandException("Your town does not own the chunk at your location.");
		}
	}
}
