/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.command.module;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.util.GitRepositoryState;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Admin
 */
@AnnotatedCommand(name = "version", usage = "version", desc = "Show the plugin's version.", permission = "communities.dev.version", server = true, listInHelp = false)
public class ShowVersion extends CommandModule {

	private GitRepositoryState getGitRepositoryState() throws IOException {
		InputStream stream = getClass().getClassLoader().getResourceAsStream("git.properties");
		Properties properties = new Properties();
		if (stream == null) {
			throw new FileNotFoundException("Could not find git.properties at the root of the ClassLoader.");
		}

		properties.load(stream);
		GitRepositoryState gitRepositoryState = new GitRepositoryState(properties);

		return gitRepositoryState;
	}

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		List<String> output = new ArrayList<String>();
		try {
			GitRepositoryState gitRepositoryState = getGitRepositoryState();
			output.add(String.format(ChatColor.DARK_GREEN + "Updated:" + ChatColor.GREEN + " %s", gitRepositoryState.commitTime));
			output.add(String.format(ChatColor.DARK_GREEN + "Version:" + ChatColor.GREEN + " %s %s", gitRepositoryState.branch, gitRepositoryState.commitIdAbbrev));
			output.add(String.format("" + ChatColor.BLUE + ChatColor.UNDERLINE + "%s", gitRepositoryState.getCommitGihubUrl(githubRepoUrl)));
		} catch (IOException e) {
			output.add(String.format("Unable to check version."));
			plugin.log(Level.WARNING, e.getMessage());
		}

		for (String line : output) {
			sender.sendMessage(line);
		}
	}
	public static String githubRepoUrl = "https://github.com/Betterverse/Communities";
}
