package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "kick", usage = "kick <player>", desc = "Kick a player from your town", permission = "communities.normal.kick", min = 2, max = 2, community = true, rank = CommunityRank.ASSISTANT)
public class KickMember extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = player.getCommunity();
		if (community.isMember(args[1])) {
			CommunityPlayer kick = plugin.fromBukkitPlayer(args[1]);
			if (!kick.isMayor()) {
				community.removePlayer(args[1]);
				sender.sendMessage(ChatColor.GREEN + "You have kicked " + ChatColor.YELLOW + args[1] + ChatColor.GREEN + " from your town.");

				// Notify the kicked player if online
				Player kicked = plugin.getServer().getPlayer(args[1]);
				if (kicked != null) {
					kicked.sendMessage(ChatColor.RED + "You have been kicked from your town!");
				}
			} else {
				throw new CommandException("The mayor cannot be kicked from the town.");
			}
		} else {
			throw new CommandException(args[1] + " is not a member of your town.");
		}
	}
}
