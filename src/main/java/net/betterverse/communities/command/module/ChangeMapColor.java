package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;

@AnnotatedCommand(name = "color", usage = "color <color>", desc = "Set the color of your town", permission = "communities.normal.color", min = 2, max = 2, community = true, rank = CommunityRank.MAYOR)
public class ChangeMapColor extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		Community community = player.getCommunity();
		if (isValidColor(args[1])) {
			community.setMapColor(args[1]);
			sender.sendMessage(ChatColor.GREEN + "Your town will now render in the color " + ChatColor.YELLOW + args[1] + ChatColor.GREEN + " on the dynamic map.");
		} else {
			throw new CommandException("Invalid color. Color must be a valid hexidecimal (#00FF00).");
		}
	}

	@Override
	public boolean canExecute() {
		return plugin.config().isColorChangeEnabled();
	}

	private boolean isValidColor(String color) {
		if (color.length() == 7 && color.charAt(0) == '#') {
			for (int i = 1; i < color.length(); i++) {
				char colorChar = color.charAt(i);
				if (!Character.isDigit(colorChar) && !Character.isWhitespace(colorChar) && "0123456789abcdefABCDEF".indexOf(colorChar) < 0) {
					return false;
				}
			}

			return true;
		}

		return false;
	}
}
