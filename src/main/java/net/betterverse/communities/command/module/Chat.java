package net.betterverse.communities.command.module;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import net.betterverse.communities.command.AnnotatedCommand;
import net.betterverse.communities.command.CommandException;
import net.betterverse.communities.command.CommandModule;
import net.betterverse.communities.community.CommunityPlayer;

@AnnotatedCommand(name = "chat", usage = "chat", desc = "Toggle town chat", permission = "communities.normal.chat", max = 1, community = true)
public class Chat extends CommandModule {

	@Override
	public void execute(CommandSender sender, CommunityPlayer player, String[] args) throws CommandException {
		if (player.getCommunity().toggleTownChat(sender.getName())) {
			sender.sendMessage(ChatColor.GREEN + "You are now in town chat! Only other town members can hear you.");
		} else {
			sender.sendMessage(ChatColor.GREEN + "You have left town chat.");
		}
	}
}
