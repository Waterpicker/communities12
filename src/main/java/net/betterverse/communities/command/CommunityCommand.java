package net.betterverse.communities.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.betterverse.communities.Communities;
import net.betterverse.communities.command.module.*;
import net.betterverse.communities.community.CommunityPlayer;
import net.betterverse.communities.community.CommunityRank;
import net.betterverse.communities.util.Paginator;
import net.betterverse.communities.util.StringHelper;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommunityCommand implements CommandExecutor {

	private final Communities plugin;
	private final Map<String, CommandModule> modules = new HashMap<String, CommandModule>();

	public CommunityCommand(Communities plugin) {
		this.plugin = plugin;

		registerModule(AcceptInvitation.class);
		registerModule(BuyChunk.class);
		registerModule(ChangeMapColor.class);
		registerModule(Chat.class);
		registerModule(ClaimChunk.class);
		registerModule(CreateTown.class);
		registerModule(DeclineInvitation.class);
		registerModule(DeleteTown.class);
		registerModule(Deposit.class);
		registerModule(Description.class);
		registerModule(ExemptPaying.class);
		registerModule(FlagMonsterSpawn.class);
		registerModule(ForceUnclaimChunk.class);
		registerModule(Invite.class);
		registerModule(KickMember.class);
		registerModule(LeaveTown.class);
		registerModule(Reload.class);
		registerModule(RemoveTown.class);
		registerModule(SetAssistant.class);
		registerModule(ListChunkGroups.class);
		registerModule(CreateChunkGroup.class);
		registerModule(SetChunkGroup.class);
		registerModule(SetHome.class);
		registerModule(SetMayor.class);
		registerModule(SetOnSale.class);
		registerModule(SetTax.class);
		registerModule(ShowMap.class);
		registerModule(ShowVersion.class);
		registerModule(TeleportHome.class);
		registerModule(ToggleBlockEditor.class);
		registerModule(TownInfo.class);
		registerModule(TownList.class);
		registerModule(UnclaimChunk.class);
		registerModule(UnSell.class);
		registerModule(ViewBalance.class);
		registerModule(Withdraw.class);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {
		CommunityPlayer comPlayer = null;
		if (sender instanceof Player) {
			comPlayer = plugin.fromBukkitPlayer(((Player) sender).getName());
		}
		if (args.length > 0) {
			if (modules.containsKey(args[0].toLowerCase())) {
				AnnotatedCommand annotatedCmd = modules.get(args[0].toLowerCase()).getClass().getAnnotation(AnnotatedCommand.class);
				if (!annotatedCmd.server() && !(sender instanceof Player)) {
					sender.sendMessage("Only in-game players may use that command.");
					return true;
				}
				if (hasPermission(sender, comPlayer, annotatedCmd)) {
					if (!annotatedCmd.community() || (annotatedCmd.community() && comPlayer.getCommunity() != null)) {
						try {
							if (args.length >= annotatedCmd.min() && (annotatedCmd.max() == -1 || args.length <= annotatedCmd.max())) {
								CommandModule module = modules.get(args[0].toLowerCase());
								if (module.canExecute()) {
									module.execute(sender, comPlayer, args);
								} else {
									sender.sendMessage(ChatColor.RED + "This command has been disabled.");
								}
							} else {
								throw new CommandUsageException();
							}
						} catch (NumberFormatException e) {
							sender.sendMessage(ChatColor.RED + "Invalid number.");
						} catch (CommandUsageException e) {
							sender.sendMessage(ChatColor.RED + "Invalid arguments. Usage: /" + cmdLabel + " " + annotatedCmd.usage());
						} catch (CommandException e) {
							sender.sendMessage(ChatColor.RED + e.getMessage());
						} catch (Exception e) {
							sender.sendMessage(ChatColor.RED + "A mysterious error has occured (see console): " + e.getMessage());
							sender.sendMessage(ChatColor.RED + "Please report this error. Be sure to give as much information as possible.");
							e.printStackTrace();
						}
					} else {
						sender.sendMessage(ChatColor.RED + "You must be part of a community to use that command.");
					}
				} else {
					sender.sendMessage(ChatColor.RED + "You do not have permission.");
				}
			} else if (args[0].equalsIgnoreCase("help") || args[0].equals("?")) {
				// Attempt to display help
				int page = 1;
				if (args.length == 2) {
					try {
						page = Integer.parseInt(args[1]);
					} catch (NumberFormatException e) {
						sender.sendMessage(ChatColor.RED + "Invalid number.");
						return true;
					}
				}
				sendHelp(sender, comPlayer, cmdLabel, page);
			} else {
				sendHelp(sender, comPlayer, cmdLabel, 1);
			}
		} else {
			sendHelp(sender, comPlayer, cmdLabel, 1);
		}

		return true;
	}

	private List<AnnotatedCommand> getAvailableCommands(CommandSender sender, CommunityPlayer player) {
		List<AnnotatedCommand> result = new ArrayList<AnnotatedCommand>();

		for (CommandModule module : modules.values()) {
			if (module.canExecute()) {
				AnnotatedCommand annotation = module.getClass().getAnnotation(AnnotatedCommand.class);
				if (hasPermission(sender, player, annotation)) {
					result.add(annotation);
				}
			}
		}

		return result;
	}

	private boolean hasPermission(CommandSender sender, CommunityPlayer player, AnnotatedCommand cmd) {
		if (player == null) {
			// Console
			return sender.isOp();
		}

		return (cmd.permission().isEmpty() || sender.hasPermission(cmd.permission())) && (cmd.rank() == CommunityRank.NONE || player.getRank().getLevel() >= cmd.rank().getLevel());
	}

	private void registerModule(Class<? extends CommandModule> clazz) {
		if (clazz.isAnnotationPresent(AnnotatedCommand.class)) {
			try {
				modules.put(clazz.getAnnotation(AnnotatedCommand.class).name(), clazz.asSubclass(CommandModule.class).newInstance());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void sendHelp(CommandSender sender, CommunityPlayer player, String cmdName, int page) {
		// Populate the help paginator with the sender's possible commands
		List<AnnotatedCommand> commands = new ArrayList<AnnotatedCommand>();
		for (AnnotatedCommand command : getAvailableCommands(sender, player)) {
			if (command.listInHelp()) {
				commands.add(command);
			}
		}

		if (commands.isEmpty()) {
			sender.sendMessage(ChatColor.RED + "You do not have permission to use any Communities commands.");
		} else {
			Paginator paginator = new Paginator();
			paginator.header("&6                    " + ChatColor.UNDERLINE + "Communities Help" + ChatColor.RESET + " <<page>/<max-pages>>").description("    &a** Type " + "&e/coms help (page) &afor more. **");
			for (AnnotatedCommand annotation : commands) {
				paginator.addLine(StringHelper.parseColors("&7/&9" + cmdName + " &e" + annotation.usage() + " &7-&f " + ChatColor.ITALIC + annotation.desc()));
			}

			paginator.sendPage(sender, page);
		}
	}
}
