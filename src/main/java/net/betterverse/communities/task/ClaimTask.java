/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.task;

import java.util.*;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.WorldCoord;
import net.betterverse.communities.event.CommunityClaimChunkEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.util.Vector;

/**
 *
 * @author Admin
 */
public abstract class ClaimTask extends SelectionTask {

	Community community;

	public ClaimTask(Community community, Location cursorLocation) {
		this.community = community;
		this.cursorWorld = cursorLocation.getWorld();
		this.cursorVector = cursorLocation.toVector();
	}

	public Community getCommunity() {
		return community;
	}

	public void sendResults() {
		sendToSender("Removed (Filtered Out): %d", removed);
		sendToSender("Attempted: %d", attempted);
		sendToSender("Succeded: %d", succceded);
		sendToSender("Failed: %d", failed);
	}

	@Override
	public void run() {
		loadSelection();

		if (Bukkit.getPluginManager() != null) {
			CommunityClaimChunkEvent event = new CommunityClaimChunkEvent(this);
			Bukkit.getPluginManager().callEvent(event);
			if (event.isCancelled()) {
				return;
			}
		}

		attemptToClaimSelection();
		sendResults();
	}

	public void attemptToClaimSelection() {
		for (WorldCoord coord : selection) {
			attempted++;
			if (community.attemptToBuyChunk()) {
				community.addToTerritory(coord);
				succceded++;
			} else {
				break;
			}
		}

		failed = selection.size() - succceded;
	}
}
