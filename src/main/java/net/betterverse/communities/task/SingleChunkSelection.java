/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.task;

import net.betterverse.communities.community.WorldCoord;

/**
 *
 * @author Admin
 */
public interface SingleChunkSelection extends ChunkSelection {

	public WorldCoord getFirstSelection();
}
