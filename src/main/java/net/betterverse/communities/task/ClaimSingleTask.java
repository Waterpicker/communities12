/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.task;

import java.util.ArrayList;
import java.util.List;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.Coord;
import net.betterverse.communities.community.WorldCoord;
import net.betterverse.communities.event.CommunityClaimChunkEvent;
import org.bukkit.*;

/**
 *
 * @author Admin
 */
public class ClaimSingleTask extends ClaimTask implements SingleChunkSelection {

	public ClaimSingleTask(Community community, Location cursorLocation) {
		super(community, cursorLocation);
	}

	public WorldCoord getFirstSelection() {
		return selection.iterator().next();
	}

	@Override
	public void loadSelection() {
		WorldCoord coord = new WorldCoord(cursorWorld.getName(), Coord.parseCoord(cursorVector.getBlockX(), cursorVector.getBlockZ()));
		addToSelection(coord);
	}

	@Override
	public void sendResults() {
		if (succceded > 0) {
			sendToSender(ChatColor.GREEN + "You have claimed the chunk at your location for your town!");
		}
	}
}
