/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.model;

import com.iciql.Iciql.IQColumn;
import com.iciql.Iciql.IQTable;

/**
 *
 * @author Admin
 */
@IQTable
public class Community {

	@IQColumn(primaryKey = true, autoIncrement = true)
	public long id;
	@IQColumn(nullable = false, length = 64)
	public String name;
}
