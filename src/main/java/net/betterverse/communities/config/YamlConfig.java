/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author Admin
 */
public class YamlConfig extends YamlConfiguration {

	File file;

	public YamlConfig(File file) {
		setFile(file);
		options().indent(4);
	}

	private void setFile(File file) {
		this.file = file;
	}

	private void createFilepathIfNotFound() throws IOException {
		if (!file.exists()) {
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			file.createNewFile();
		}
	}

	public void dumpByAnnotation() {
		for (Field field : getClass().getDeclaredFields()) {
			Node node = field.getAnnotation(Node.class);
			if (node == null) {
				continue;
			}

			try {
				Object obj = field.get(this);
				set(node.value(), obj);
			} catch (Exception ex) {
				Logger.getLogger(YamlConfig.class.getName()).log(Level.SEVERE, null, ex);
				continue;
			}
		}
	}

	public void save() throws IOException {
		dumpByAnnotation();
		createFilepathIfNotFound();
		save(file);
	}

	public void load() throws FileNotFoundException, IOException, InvalidConfigurationException {
		createFilepathIfNotFound();
		load(file);
		loadByAnnotation();
	}

	public void loadByAnnotation() {
		for (Field field : getClass().getFields()) {
			Node node = field.getAnnotation(Node.class);
			if (node == null) {
				continue;
			}

			try {
				Object def = field.get(this);
				Object value = get(node.value(), def);
				field.set(this, value);
			} catch (Exception ex) {
				Logger.getLogger(YamlConfig.class.getName()).log(Level.SEVERE, null, ex);
				continue;
			}
		}
	}

	public void update() {
		try {
			load();
			save();
		} catch (Exception ex) {
			Logger.getLogger(YamlConfig.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
