/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.event;

import java.util.Set;
import net.betterverse.communities.community.Community;
import net.betterverse.communities.community.WorldCoord;
import net.betterverse.communities.task.ClaimTask;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Admin
 */
public class CommunityClaimChunkEvent extends Event implements Cancellable {

	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled = false;
	private ClaimTask claimTask;

	public CommunityClaimChunkEvent(ClaimTask claimTask) {
		this.claimTask = claimTask;
	}

	public Community getCommunity() {
		return getClaimTask().getCommunity();
	}

	public CommandSender getSender() {
		return getClaimTask().getSender();
	}

	public ClaimTask getClaimTask() {
		return claimTask;
	}

	public Set<WorldCoord> getSelection() {
		return getClaimTask().getSelection();
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancel) {
		this.cancelled = cancel;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
