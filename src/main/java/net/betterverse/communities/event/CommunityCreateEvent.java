/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.event;

import net.betterverse.communities.community.WorldCoord;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 *
 * @author Admin
 */
public class CommunityCreateEvent extends Event implements Cancellable {

	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled = false;
	CommandSender sender;
	private String communityName;
	private Location location;

	public CommunityCreateEvent(CommandSender sender, String communityName, Location location) {
		this.sender = sender;
		this.communityName = communityName;
		this.location = location;
	}

	public CommandSender getSender() {
		return sender;
	}

	public String getCommunityName() {
		return communityName;
	}

	public Location getLocation() {
		return location;
	}

	public WorldCoord getInitialCoord() {
		return WorldCoord.parseWorldCoord(getLocation());
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancel) {
		this.cancelled = cancel;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
