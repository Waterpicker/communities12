/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.betterverse.communities.event;

import net.betterverse.communities.community.Community;
import org.bukkit.event.Event;

/**
 *
 * @author Admin
 */
public abstract class CommunityEvent extends Event {

	private Community community;

	public CommunityEvent(Community community) {
		this.community = community;
	}

	public Community getCommunity() {
		return community;
	}
}
